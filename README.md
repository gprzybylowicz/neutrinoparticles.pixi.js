# neutrinoparticles.pixi

The package allows you to render [NeutrinoParticles](https://neutrinoparticles.com/) effects in [PIXI](https://www.pixijs.com/) graphical framework.

## PIXI v4 or v5?

 * Versions `1.x.x` are for PIXI v4. [go to branch](https://gitlab.com/neutrinoparticles/neutrinoparticles.pixi.js/tree/1.x.x)
 * **Versions `>=2.0.0` are for PIXI v5. (CURRENT DOCS)**

## API documentation

For more info about latest API you can check [API documentation](https://neutrinoparticles.gitlab.io/neutrinoparticles.pixi.js-doc/master/) pages.

For specific library version refer to [Releases](https://gitlab.com/neutrinoparticles/neutrinoparticles.pixi.js/-/releases) page.

## Installation

You can install the package with `npm`:
```
> npm install neutrinoparticles.pixi
```
Or download pre-built package at [Releases](https://gitlab.com/neutrinoparticles/neutrinoparticles.pixi.js/-/releases) page. There are [UMD](https://www.davidbcalhoun.com/2014/what-is-amd-commonjs-and-umd/) packages which you can use in any environment.

## Quick usage

Acquire a package object. Depending on your environment:

* HTML
```html
<script src="path/to/neutrinoparticles.pixi/dist/neutrinoparticles.pixi.umd.js"></script>
```
* node.js
```javascript
var PIXINeutrino = require('neutrinoparticles.pixi')
```
* ES6
```javascript
import * as PIXINeutrino from 'neutrinoparticles.pixi'
```

Register `PIXINeutrino` plugins (before creating `PIXI.Application`):
```javascript
PIXINeutrino.registerPlugins();
```

Specify additional `neutrino` options when creating `PIXI.Application`:
```javascript
let app = new PIXI.Application({
  width: 800,
  height: 600,
  neutrino: {
    texturesBasePath: 'textures/' // Prefix for textures
  }
});
```
Load effect:

```javascript
app.loader
	.add('water_stream', 'path/to/effects/water_stream.js', app.neutrino.loadOptions)
	.load((loader, resources) =>
{
  ...
```

Add effect on the scene:

```javascript
  ...

  let effect = new PIXINeutrino.Effect(resources.water_stream.effectModel, {
	  position: [400, 300, 0]
  });

  app.stage.addChild(effect);

  ...
```

And make it updating:

```javascript
  ...

  app.ticker.add((delta) => {
	  const msec = delta / PIXI.settings.TARGET_FPMS;
	  const sec = msec / 1000.0;

	  effect.update(sec);
  });
});
```

## Tutorials
* [Basic Usage](./tutorials/basic_usage.md)
* [Using turbulence (or noise)](./tutorials/turbulence.md)
* [Scene hierarchy, rotation and scale](./tutorials/scene_hierarchy_rotation_and_scale.md)

## Samples
There are several samples in folder `/samples` which you can refer to.

#### Running samples
To run the samples you will need web server running on localhost:80 with a web root on the root repository folder. If you have Python3 installed you can use `/start_http_server.sh` script to start it.

After server is running you can access the samples by `http://localhost/samples/<sample_name>.html`
> For different web server configuration, please adjust the path above accordingly.

#### Debug Samples with VSCode

You can use following launch configuration to start debuging of samples:
```json
{
    "name": "Samples in Chrome",
    "type": "chrome",
    "runtimeExecutable": "/path/to/chrome",
    "runtimeArgs": [
        "--incognito",
        "--new-window",
        "--remote-debugging-port=9222",
        "--user-data-dir=remote-profile"
    ],
    "request": "launch",
    "url": "http://localhost/samples/simplest.html",
    "webRoot": "${workspaceFolder}",
    "breakOnLoad": true,
    "sourceMaps": true,
    "port": 9222,
}
```
> To be able to set breakpoint in .html files you will need to enable `Allow Breakpoints Everywhere` flag in VSCode Settings.

## Tests
#### Running tests

To run tests you can use `npm run test` command as usual. It will start IBCT (image based comparison tests) in WebGL and Canvas modes. You will need to provide GPU environment to run the tests. It can be hardware video card or software renderer.

#### Debug with VSCode

You can use following launch configuration to start debugging of main thread of electron-mocha application which perform the tests:

```json
{
	"name": "Debug Tests",
	"type": "node",
	"request": "launch",
	"cwd": "${workspaceRoot}",
	"runtimeExecutable": "${workspaceRoot}/node_modules/.bin/electron-mocha",
	"windows": {
	  "runtimeExecutable": "${workspaceRoot}/node_modules/.bin/electron-mocha.cmd"
	},
	"program": "${workspaceRoot}/test/test.js",
	"args": [
		"--require", "@babel/register",
		"--timeout", "999999",
		"--colors",
		"--ibct-expect-dir", "__my_expect__",
		"test/test.js"
	],
	"console": "integratedTerminal",
	"protocol": "inspector",
}
```
> To be able to set breakpoint in .html files you will need to enable `Allow Breakpoints Everywhere` flag in VSCode Settings.

Please note, that `ibct-expect-dir` argument overrides default expectation files directory `__expect__` for IBCT. The directory is placed in /test/ibct. Such behaviour is useful when running tests locally, because different hardware gives different results and default expectations will most probably fail for you.

## Contribution
We will be much appreciated for any fix/suggestion/feature merge requests and will consider them ASAP.

There are not much rules for such requests, just be sure that all tests are passing.
