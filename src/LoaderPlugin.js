import { EffectModel } from './EffectModel.js';
import { PIXITexturesLoader } from './TexturesLoader'

const _pixiTexturesLoader = new PIXITexturesLoader();

/**
 * Plugin for PIXI.Loader.
 * Usually, it is installed by {@link registerPlugins}.
 */
export class LoaderPlugin
{
    static use(resource, next)
    {
        if (resource.extension === 'js' 
            && resource.metadata
            && resource.metadata.neutrino
            && resource.data)
        {
            _pixiTexturesLoader.setPIXILoader(this);
            _pixiTexturesLoader.setParentResource(resource);
            resource.effectModel = new EffectModel(resource.metadata.neutrino, resource.data, _pixiTexturesLoader);
        }

        next();
    }
}
