/**
 * Enum for pause type. Used in {@link Effect} constructor.
 * 
 * @static
 * @constant
 * @type {object}
 * @property {Symbol} NO - Not paused. Effect will start immidiately. Passed 
 * starting position and rotation considered as global and will be used 
 * to start simulate effect. Please note, that this pause type is not suitable
 * for effects you want to attach to other containers.
 * @property {Symbol} BEFORE_UPDATE_OR_RENDER - Paused until the first update or render.
 * Effect will be paused until its first update or render call. This pause type
 * is useful in most of cases when you don't want to pause effect. 
 * It allows to place effect on the scene in several subsequent methods calls:
 * create effect, attach to some parent container and set position of
 * the effect in container.
 * @property {Symbol} YES - Effect paused until unpause() called.
 */
export const Pause = Object.freeze({
	NO: Symbol(0),
	BEFORE_UPDATE_OR_RENDER: Symbol(1),
	YES: Symbol(2)
})
