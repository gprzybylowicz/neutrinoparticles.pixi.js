/**
 * Texture loader interface used by {@link EffectModel} to load necessary textures.
 */
export class AbstractTexturesLoader
{
    constructor() {
    }

    /**
	 * Requests loading of a texture.
	 * 
	 * @param {string} texturePath Path to texture.
	 * @param {function(texture)} callback Should be called when texture is loaded.
	 */
    load(texturePath, callback) {
    }
}

export class PIXITexturesLoader
{
    constructor() {
        this._pixiLoader = null;
        this._parentResource = null;
    }

    setPIXILoader(pixiLoader) {
        this._pixiLoader = pixiLoader;
    }

    setParentResource(parentResource) {
        this._parentResource = parentResource;
    }

    load(texturePath, callback) {
        this._pixiLoader.add(texturePath, 
            { 
                parentResource: this._parentResource 
            }, 
            function (resource) 
            {
                callback(resource.texture);
            });
    }    
}