import { ApplicationPlugin } from './AplicationPlugin';
import { RendererPlugin } from './RendererPlugin';
import { LoaderPlugin } from './LoaderPlugin';

/**
 * Registers all plugins in PIXI. Namely {@link ApplicationPlugin}, {@link RendererPlugin} and
 * {@link LoaderPlugin}.
 * 
 * Should be called before PIXI.Application created.
 */
export function registerPlugins()
{
    PIXI.Application.registerPlugin(ApplicationPlugin)
    PIXI.Renderer.registerPlugin('neutrino', RendererPlugin);
    PIXI.Loader.registerPlugin(LoaderPlugin);
}

export * from './const';
export * from './Context';
export * from './Effect';
export * from './EffectModel';
export * from './QuadBuffer';
export * from './TexturesLoader';
export * from './PerspectiveProjection';
export {
    ApplicationPlugin,
    RendererPlugin,
    LoaderPlugin
}

