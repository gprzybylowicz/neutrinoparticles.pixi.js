const _tempV3 = [0, 0, 0];

export class QuadBuffer 
{
  constructor(pushQuadCallback, projection) 
  {
    this.worldTransform = [0, 0, 0, 0, 0, 0];
    this.worldAlpha = 1.0;

    this._pushQuadCallback = pushQuadCallback;

    function Vertex() {
      this.position = [0, 0];
      this.color = [0, 0, 0, 0];
      this.texCoords = [0, 0];
    }

    this._vertices = [ 
      new Vertex(),
      new Vertex(),
      new Vertex(),
      new Vertex()
    ];

    this._currentVertex = 0;
    this._renderStyle = 0;

    this._projection = projection;
    this._quadDiscarded = false;
  }

  initialize(maxNumVertices, texChannels, indices, maxNumRenderCalls) 
  {
  }

  beforeQuad(renderStyle) 
  {
    this._renderStyle = renderStyle;
    this._quadDiscarded = false;
  }

  pushVertex(vertex) 
  {
    const v = this._vertices[this._currentVertex];

    const wt = this.worldTransform;

    const x = vertex.position[0];
    const y = vertex.position[1];

    _tempV3[0] = wt[0] * x + wt[2] * y + wt[4];
    _tempV3[1] = wt[1] * x + wt[3] * y + wt[5];

    if (this._projection) {
      _tempV3[2] = vertex.position[2];

      this._quadDiscarded |= 
        !this._projection.transformPosition(v.position, _tempV3);
    } else {
      v.position[0] = _tempV3[0];
      v.position[1] = _tempV3[1];
    }

    v.color[0] = vertex.color[0];
    v.color[1] = vertex.color[1];
    v.color[2] = vertex.color[2];
    v.color[3] = vertex.color[3] * this.worldAlpha;

    v.texCoords[0] = vertex.texCoords[0][0];
    v.texCoords[1] = vertex.texCoords[0][1];

    ++this._currentVertex;

    if (this._currentVertex == 4)
    {
      if (!this._quadDiscarded) {
        this._pushQuadCallback(this._vertices, this._renderStyle);
      }

      this._currentVertex = 0;
    }
  }

  pushRenderCall(rc) 
  {
  }

  cleanup() 
  {
  }
}
