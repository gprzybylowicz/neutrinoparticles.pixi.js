import { Context } from './Context';

/**
 * Application plugin which will create {@link Context} object
 * and assign it to PIXI.Application.neutrino property of the application.
 * 
 * Usually installed by {@link registerPlugins}.
 * 
 * @param {Object} options Options
 * @param {Object} options.neutrino Options object to pass to {@link Context} constructor.
 */
export class ApplicationPlugin
{
    static init(options)
    {
        this.neutrino = new Context(this.renderer, options.neutrino || {});
    }

    static destroy()
    {
        this.neutrino = null;
    }
}
