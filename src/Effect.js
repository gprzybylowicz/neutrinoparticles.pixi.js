import { QuadBuffer } from './QuadBuffer';
import { EffectModel } from './EffectModel';
import { Pause } from './const';
import * as PIXI from 'pixi.js';

const _cancamPoint0 = new PIXI.Point(0, 0);
const _cancamPoint1 = new PIXI.Point(0, 0);
const _cancamPoint2 = new PIXI.Point(0, 0);
const _cancamArray0 = [0, 0, 0];
const _rencanMatrix0 = new PIXI.Matrix();

/**
 * Represents an instance of effect on the scene.
 * 
 * The class is a wrapper around exported NeutrinoParticles effect. It
 * introduces everything to place effect on a PIXI scene.
 * 
 * @example
 * app.loader
 * // Load effect using PIXI's loader. And don't forget to specify loadOptions
 * .add('my_effect', 'path/to/my/effect.js', app.neutrino.loadOptions)
 * .load((loader, resources) => 
 * {
 *   // Create effect instance using loaded model
 *   let effect = new PIXINeutrino.Effect(resources.my_effect.effectModel, {
 *     position: [400, 300, 0]
 *   });
 *   // Add effect on the scene
 *   app.stage.addChild(effect);
 * 	 // Update effect with PIXI's ticker
 *   app.ticker.add((delta) => {
 * 	   const msec = delta / PIXI.settings.TARGET_FPMS;
 *     const sec = msec / 1000.0;
 * 	   effect.update(sec);
 *   });
 * });
 * 
 * @param {EffectModel} effectModel A model which is used to instantiate and simulate effect.
 * @param {Object} [options={}] Options
 * @param {Array} [options.position=[0, 0, 0]] Location of the effect on a scene.
 * @param {number} [options.rotation=0] Rotation of the effect in radians. 
 * @param {Array} [options.scale=[1, 1, 1]]  Scale by axes for the effect.
 * @param {Pause} [options.pause=Pause.BEFORE_UPDATE_OR_RENDER] Pause type on start.
 * @param {boolean} [options.generatorsPaused=false] Generators paused on start.
 * @param {PIXI.Container} [options.baseParent=null] An object which defines global coordinate
 *   system for the effect. By default, identity coordinate system used.
 * @param {PerspectiveProjection} [options.projection=null] Projection to use in the effect. If null - the
 *   effect will be rendered using orthogonal projection (by using only X and Y values of vertices).
 */
export class Effect extends PIXI.Container {

	constructor(effectModel, options, arg3, arg4, arg5) {
		super();

		// Support for constructor(effectModel, position, rotation, scale, baseParent)
		if (options instanceof Array) {
			options = {
				position: options,
				rotation: arg3 || 0,
				scale: arg4 || [1, 1, 1],
				baseParent: arg5 || null
			};
		}

		options = Object.assign({
			position: [0, 0, 0],
			rotation: 0,
			scale: [1, 1, 1],
			pause: Pause.BEFORE_UPDATE_OR_RENDER,
			generatorsPaused: false,
			baseParent: null,
			projection: null
		}, options);

		/** 
		 * {@link Context} used to create the effect. 
		 * @member {Context}
		 */
		this.ctx = effectModel.ctx;

		/** 
		 * {@link EffectModel} used to create the effect. 
		 * @member {EffectModel}
		 */
		this.effectModel = effectModel;

		/** 
		 * Underlying {@link https://gitlab.com/neutrinoparticles/neutrinoparticles.js/ | neutrinoparticles.js}
		 * effect object. 
		 * @member {object}
		 */
		this.effect = null;

		/**
		 * Container which is used as global coordinate system provider.
		 * It must be one of parent containers of the effect.
		 * @member {PIXI.Container}
		 */
		this.baseParent = options.baseParent;

		this.position.set(options.position[0], options.position[1]);

		/** 
		 * Z coordinate of position. X and Y coordinates are used from PIXI.Container.position. 
		 * @member {number}
		 */
		this.positionZ = options.position[2];

		this.rotation = options.rotation;

		this.scale.x = options.scale[0];
		this.scale.y = options.scale[1];

		/** 
		 * Z coordinate of scale vector. X and Y coordinates are used from PIXI.Container.scale. 
		 * @member {number}
		 */
		this.scaleZ = options.scale[2];

		this._projection = options.projection;

		this._renderElements = [];
		this._startupOptions = {
			paused: options.pause !== Pause.NO,
			generatorsPaused: options.generatorsPaused
		};
		this._unpauseOnUpdateRender = (options.pause === Pause.BEFORE_UPDATE_OR_RENDER);

		if (effectModel.ready()) {
			this._onEffectReady();
		} else {
			effectModel.once('ready', function () {
				this._onEffectReady();
			}, this);
		}

		this._updateWorldTransform();

		if (effectModel.ctx.canvasRenderer && this._projection) {
			const _this = this;
			this._canvasCameraAdapter = {
				matrix: null,
				transform: function(positionArray, size) {
					const worldPosition = _cancamPoint1;
					const position = _cancamPoint0;
					position.x = positionArray[0];
					position.y = positionArray[1];
					const projPosition = _cancamArray0;
					const projLocalPosition = _cancamPoint2;

					this.matrix.apply(_cancamPoint0, worldPosition);

					projPosition[0] = worldPosition.x;
					projPosition[1] = worldPosition.y;
					projPosition[2] = positionArray[2];
					const result = _this._projection.transformPosition(projPosition, projPosition);

					this.matrix.applyInverse({x: projPosition[0], y: projPosition[1]},
						projLocalPosition);

					positionArray[0] = projLocalPosition.x;
					positionArray[1] = projLocalPosition.y;

					_this._projection.transformSize(size, projPosition, size);

					return result;
				}
			}
		}
	}

	/**
	 * Returns true if the effect is ready to update/render. 
	 * 
	 * Basically that means that {@link Effect#effectModel} is loaded 
	 * and all textures required to render this effect are also loaded.
	 * 
	 * @returns {boolean} True if the effect can be rendered.
	 */
	ready() {
		return this._ready;
	}

	/**
	 * Simulates effect for specified time.
	 * 
	 * @example
	 * let effect = new PIXINeutrino.Effect(resources.some_effect.effectModel);
	 * app.ticker.add((delta) => {
	 * 	 const msec = delta / PIXI.settings.TARGET_FPMS;
	 *   const sec = msec / 1000.0;
	 *   effect.update(sec);
	 * });
	 * 
	 * @param {number} seconds Time in seconds.
	 */
	update(seconds) {
		if (!this.ready())
			return;

		this._checkUnpauseOnUpdateRender();

		this._updateWorldTransform();
		
		if (this.effect != null) {
			this.effect.update(seconds, this._scaledPosition(),
				this.ctx.neutrino.axisangle2quat_([0, 0, 1], this.worldRotationDegree));
		}
	}

	/**
	 * Restarts the effect.
	 * 
	 * @param {Array} position A new position of the effect or null to keep the same position.
	 * @param {number} rotation A new rotation of the effect of null to keep the same rotation.
	 */
	restart(position, rotation) {
		if (position) {
			this.position.x = position[0];
			this.position.y = position[1];
			this.positionZ = position[2];
		}

		if (rotation) {
			this.rotation = rotation;
		}

		this._updateWorldTransform();

		this.effect.restart(this._scaledPosition(),
			rotation ? this.ctx.neutrino.axisangle2quat_([0, 0, 1], this.worldRotationDegree) 
			: null);
	}

	/**
	 * Instant change of position/rotation of the effect. 
	 * 
	 * While {@link Effect#update} changes position 
	 * of the effect smoothly interpolated, this function will prevent such smooth trail
	 * from previous position and "teleport" effect instantly.
	 * 
	 * @param {Array} position A new position of the effect or null to keep the same position.
	 * @param {number} rotation A new rotation of the effect of null to keep the same rotation.
	 */
	resetPosition(position, rotation) {
		if (position) {
			this.position.x = position[0];
			this.position.y = position[1];
			this.positionZ = position[2];
		}

		if (rotation) {
			this.rotation = rotation;
		}

		this._updateWorldTransform();

		this.effect.resetPosition(this._scaledPosition(),
			rotation ? this.ctx.neutrino.axisangle2quat_([0, 0, 1], this.worldRotationDegree) : null);
	}

	/** Pauses the effect. All active particles will freeze. */
	pause() {
		if (this.ready())
			this.effect.pauseAllEmitters();
		else
			this._startupOptions.paused = true;
	}

	/** Unpauses the effect. */
	unpause() {
		if (this.ready())
			this.effect.unpauseAllEmitters();
		else
			this._startupOptions.paused = false;
	}

	/** 
	 * Pauses all generators in the effect. No new particles will be generated.
	 * All current particles will continue to update.
	 */
	pauseGenerators() {
		if (this.ready())
			this.effect.pauseGeneratorsInAllEmitters();
		else
			this._startupOptions.generatorsPaused = true;
	}

	/** Unpauses generators in the effect. */
	unpauseGenerators() {
		if (this.ready())
			this.effect.unpauseGeneratorsInAllEmitters();
		else
			this._startupOptions.generatorsPaused = false;
	}

	/**
	 * Sets a value of emitter property in all emitters of the effect.
	 * 
	 * @param {string} name Name of property.
	 * @param {number|Array} value Value of property. It can be a Number, 
	 * 2-, 3- or 4-component Array depending on a property you want to set.
	 */
	setPropertyInAllEmitters(name, value) {
		this.effect.setPropertyInAllEmitters(name, value);
	}

	/**
	 * @returns {number} A number of active particles in the effect.
	 */
	getNumParticles() {
		return this.effect.getNumParticles();
	}

	_render(renderer)
    {
        if (!this.ready())
			return;

		this._renderer = renderer;
	  
		this._checkUnpauseOnUpdateRender();

		if (this._projection) {
			this._projection.setScreenFrame && this._projection.setScreenFrame(renderer.projection.destinationFrame);
		}

		renderer.batch.setObjectRenderer(renderer.plugins.neutrino);

		const rb = this.renderBuffers;
		const wt = rb.worldTransform;

		if (this.baseParent)
		{
			const sx = this.worldScale.x;
			const sy = this.worldScale.y;
			const m = this.baseParent.worldTransform;
			wt[0] = m.a * sx;
			wt[1] = m.b * sy;
			wt[2] = m.c * sx;
			wt[3] = m.d * sy;
			wt[4] = m.tx * sx;
			wt[5] = m.ty * sy;
		} else {
			wt[0] = this.worldScale.x;
			wt[1] = 0;
			wt[2] = 0;
			wt[3] = this.worldScale.y;
			wt[4] = 0;
			wt[5] = 0;
		}

		rb.worldAlpha = this.worldAlpha;

		this.effect.fillGeometryBuffers([1, 0, 0], [0, -1, 0], [0, 0, -1]);

		this._renderer = null;
    }

	_renderCanvas(renderer)
	{
		if (!this.ready())
			return;

		this._checkUnpauseOnUpdateRender();

		const matrix = _rencanMatrix0;

		if (this.baseParent)
		{
			this.baseParent.worldTransform.copyTo(matrix);
			matrix.scale(this.worldScale.x, this.worldScale.y);
			renderer.context.setTransform(matrix.a, matrix.b, matrix.c, matrix.d, matrix.tx, matrix.ty);
		} 
		else 
		{
			matrix.set(this.worldScale.x, 0, 0, this.worldScale.y, 0, 0);
			renderer.context.setTransform(matrix.a, matrix.b, matrix.c, matrix.d, matrix.tx, matrix.ty);
		}
		
		if (this._projection) {
			this._canvasCameraAdapter.matrix = matrix;
			this._projection.setScreenFrame && this._projection.setScreenFrame(renderer.screen);
		}

		renderer.context.save();

		renderer.context.globalAlpha = this.worldAlpha;
		this.effect.draw(renderer.context, this._canvasCameraAdapter);
		
		renderer.context.restore();
	}

	_scaledPosition() {
		return [this.worldPosition.x / this.worldScale.x, this.worldPosition.y / 
			this.worldScale.y, this.positionZ / this.scaleZ];
	}

	_updateWorldTransform() {
		var localPosition = new PIXI.Point(0, 0);
		var localXAxis = new PIXI.Point(1, 0);
		var localYAxis = new PIXI.Point(0, 1);

		var worldXAxis, worldYAxis;

		if (this.baseParent)
		{
			this.worldPosition = this.baseParent.toLocal(localPosition, this);
			worldXAxis = this.baseParent.toLocal(localXAxis, this);
			worldYAxis = this.baseParent.toLocal(localYAxis, this);
		} else {
			this.worldPosition = this.toGlobal(localPosition);
			worldXAxis = this.toGlobal(localXAxis);
			worldYAxis = this.toGlobal(localYAxis);
		}

		worldXAxis.x -= this.worldPosition.x;
		worldXAxis.y -= this.worldPosition.y;
		worldYAxis.x -= this.worldPosition.x;
		worldYAxis.y -= this.worldPosition.y;

		this.worldScale = {
			x: Math.sqrt(worldXAxis.x * worldXAxis.x + worldXAxis.y * worldXAxis.y),
			y: Math.sqrt(worldYAxis.x * worldYAxis.x + worldYAxis.y * worldYAxis.y),
		};

		this.worldRotationDegree = (this._calcWorldRotation(this) / Math.PI * 180) % 360;
	}

	_calcWorldRotation(obj) {
		if (obj.parent && obj.parent != this.baseParent)
			return obj.rotation + this._calcWorldRotation(obj.parent);
		else
			return obj.rotation;
	}

	_onEffectReady() {
		this._updateWorldTransform();

		if (this.effectModel.ctx.canvasRenderer) {
			this.effect = this.effectModel.effectModel.createCanvas2DInstance(this._scaledPosition(),
				this.ctx.neutrino.axisangle2quat_([0, 0, 1], this.worldRotationDegree),
				this._startupOptions);
			this.effect.textureDescs = this.effectModel.textureImageDescs;
		} else {
			this.renderBuffers = new QuadBuffer((vertices, renderStyle) => {
				this._pushQuad(vertices, renderStyle);
			}, this._projection);
			this._updateWorldTransform();
			this.effect = this.effectModel.effectModel.createWGLInstance(this._scaledPosition(),
				this.ctx.neutrino.axisangle2quat_([0, 0, 1], this.worldRotationDegree), this.renderBuffers,
				this._startupOptions);
			this.effect.texturesRemap = this.effectModel.texturesRemap;
		}

		this._ready = true;
		this.emit('ready', this);
	}

	_checkUnpauseOnUpdateRender() {
		if (this._unpauseOnUpdateRender) {
			this.resetPosition();
			this.unpause();
			this._unpauseOnUpdateRender = false;
		}
	}

	_pushQuad(vertices, renderStyleIndex)
	{
		const renderStyle = this.effect.model.renderStyles[renderStyleIndex];
		const texIndex = renderStyle.textureIndices[0];
		const texture = this.effectModel.textures[texIndex];
		const material = this.effect.model.materials[renderStyle.materialIndex];

		let blendMode;
		switch (material) 
		{
			default: blendMode = PIXI.BLEND_MODES.NORMAL; break;
			case 1: blendMode = PIXI.BLEND_MODES.ADD; break;
			case 2: blendMode = PIXI.BLEND_MODES.MULTIPLY; break;
		}

		this._renderer.plugins.neutrino.render(vertices, texture, blendMode);
	}
}
