import * as PIXI from 'pixi.js';

/**
 * Renderer plugin which knows how to batch and render particles geometry.
 * 
 * Basically, it is a modified PIXI.AbstractRendererPlugin. As a difference,
 * it has only one geometry buffer size (with maximum capacity). And all elements are
 * rendered right away without storing them to make postponed flush.
 */
export class RendererPlugin extends PIXI.ObjectRenderer {
    constructor(renderer) {
        super(renderer);

        this.options = Object.assign({
            maxBatchVertices: 16384,
            maxBatchTextures: PIXI.settings.SPRITE_MAX_TEXTURES,
            canUploadSameBuffer: PIXI.settings.CAN_UPLOAD_SAME_BUFFER
        }, renderer.options.neutrino || {});

        this.shaderGenerator = new PIXI.BatchShaderGenerator(
            PIXI.BatchPluginFactory.defaultVertexSrc,
            PIXI.BatchPluginFactory.defaultFragmentTemplate);

        this.geometryClass = PIXI.BatchGeometry;
        this.vertexSize = 6;
        this.state = PIXI.State.for2d();
        this.maxVertices = this.options.maxBatchVertices;
        this.maxIndices = this.maxVertices / 4 * 6;

        this._vertexCount = 0;
        this._indexCount = 0;
        this._shader = null;
        this._packedGeometries = [];
        this._packedGeometryPoolSize = 2;
        this._flushId = 0;
        this.MAX_TEXTURES = 1;

        this.renderer.on('prerender', this.onPrerender, this);
        renderer.runners.contextChange.add(this);

        this._dcIndex = 0;
        this._aIndex = 0;
        this._iIndex = 0;
        this._attributeBuffer = new PIXI.ViewableBuffer(this.maxVertices  * this.vertexSize * 4)
        this._indexBuffer = new Uint16Array(this.maxIndices);

        this._drawCallPool = [];
        this._textureArrayPool = [];

        this._indices = [0, 1, 3, 1, 2, 3];

        this._texArrayTick = 0;
    }

    contextChange() {
        const gl = this.renderer.gl;

        if (PIXI.settings.PREFER_ENV === PIXI.ENV.WEBGL_LEGACY) {
            this.MAX_TEXTURES = 1;
        }
        else {
            // step 1: first check max textures the GPU can handle.
            this.MAX_TEXTURES = Math.min(
                gl.getParameter(gl.MAX_TEXTURE_IMAGE_UNITS),
                this.options.maxBatchTextures);

            // step 2: check the maximum number of if statements the shader can have too..
            this.MAX_TEXTURES = PIXI.checkMaxIfStatementsInShader(
                this.MAX_TEXTURES, gl);
        }

        this._shader = this.shaderGenerator.generateShader(this.MAX_TEXTURES);

        for (let i = 0; i < this._packedGeometryPoolSize; i++) {
            this._packedGeometries[i] = new (this.geometryClass)();
        }

        const MAX_SPRITES = this.maxVertices / 4;
        // max texture arrays
        const MAX_TA = Math.floor(MAX_SPRITES / this.MAX_TEXTURES) + 1;

        while (this._drawCallPool.length < MAX_SPRITES) {
            this._drawCallPool.push(new PIXI.BatchDrawCall());
        }
        while (this._textureArrayPool.length < MAX_TA) {
            this._textureArrayPool.push(new Array());
        }
    }

    onPrerender() {
        this._flushId = 0;
        
        this._flushReset();
    }

    render(vertices, texture, blendMode) {
        const batch = this.renderer.batch;

        if (!texture.valid) {
            return;
        }

        if (this._vertexCount + 4 > this.maxVertices ||
            this._indexCount + 6 > this.maxIndices) 
        {
            this.flush();
        }

        this._vertexCount += 4;
        this._indexCount += 6;

        const {
            MAX_TEXTURES,
        } = this;

        const tex = texture.baseTexture;
        blendMode = PIXI.utils.premultiplyBlendMode[
            tex.alphaMode ? 1 : 0][blendMode];
        const touch = this.renderer.textureGC.count;

        let texArray = this._textureArrayPool[this._countTexArrays - 1];

        if (tex._neutrinoTick !== this._texArrayTick) {
            if (texArray.length >= MAX_TEXTURES) {
                texArray = this._textureArrayPool[this._countTexArrays++];
                ++this._texArrayTick;
            }

            const indexInArray = texArray.length;

            tex._neutrinoTick = this._texArrayTick;
            tex._neutrinoBatchLocation = indexInArray;
            tex.touched = touch;

            texArray.push(tex);
        }

        this.packInterleavedGeometry(vertices, this._attributeBuffer, this._indexBuffer, this._aIndex, this._iIndex,
            tex._neutrinoBatchLocation, tex.alphaMode != PIXI.ALPHA_MODES.NO_PREMULTIPLIED_ALPHA);
        this._pushDrawCall(blendMode, texArray, 4 * this.vertexSize, 6);
    }

    flush() {
        if (this._vertexCount === 0) {
            return;
        }

        this._flush();
    }

    start() {
        this.renderer.state.set(this.state);

        this.renderer.shader.bind(this._shader);

        if (this.options.canUploadSameBuffer) {
            // bind buffer #0, we don't need others
            this.renderer.geometry.bind(this._packedGeometries[this._flushId]);
        }
    }

    stop() {
        this.flush();
    }

    destroy() {
        for (let i = 0; i < this._packedGeometryPoolSize; i++) {
            if (this._packedGeometries[i]) {
                this._packedGeometries[i].destroy();
            }
        }

        this.renderer.off('prerender', this.onPrerender, this);

        this._packedGeometries = null;
        this._attributeBuffer = null;
        this._indexBuffer = null;

        if (this._shader) {
            this._shader.destroy();
            this._shader = null;
        }

        super.destroy();
    }
    
    packInterleavedGeometry(vertices, attributeBuffer, indexBuffer, aIndex, iIndex, textureId, premultiplyColor) {
        const {
            uint32View,
            float32View,
        } = attributeBuffer;

        const packedVertices = aIndex / this.vertexSize;

        const prepareColor = premultiplyColor ?
            function (srcColor) // In case of premultiplied texture
            {
                const alpha = srcColor[3];

                if (alpha == 0) 
                {
                    return 0x00000000;
                }

                if (alpha != 255) 
                {
                    const alphaNorm = alpha / 255.0;
                    return (srcColor[0] * alphaNorm) |
                        ((srcColor[1] * alphaNorm) << 8) |
                        ((srcColor[2] * alphaNorm) << 16) |
                        (alpha << 24);
                } 
                else 
                {
                    return srcColor[0] |
                        (srcColor[1] << 8) |
                        (srcColor[2] << 16) |
                        (alpha << 24);
                }
            } :
            function (srcColor)
            {
                return srcColor[0] |
                        (srcColor[1] << 8) |
                        (srcColor[2] << 16) |
                        (srcColor[3] << 24);
            }

        for (let i = 0; i < 4; ++i) {
            float32View[aIndex++] = vertices[i].position[0];
            float32View[aIndex++] = vertices[i].position[1];
            float32View[aIndex++] = vertices[i].texCoords[0];
            float32View[aIndex++] = 1.0 - vertices[i].texCoords[1];
            uint32View[aIndex++] = prepareColor(vertices[i].color);
            float32View[aIndex++] = textureId;
        }

        for (let i = 0; i < this._indices.length; i++) {
            indexBuffer[iIndex++] = packedVertices + this._indices[i];
        }
    }

    _pushDrawCall(blendMode, texArray, aSize, iSize) {
        let drawCall = this._drawCallPool[this._dcIndex];

        if (drawCall.blend !== blendMode ||
            drawCall.texArray !== texArray) {
            if (drawCall.size > 0) {
                drawCall = this._drawCallPool[++this._dcIndex];
            }

            drawCall.size = 0;
            drawCall.start = this._iIndex;
            drawCall.texArray = texArray;
            drawCall.blend = blendMode;
        }

        drawCall.size += iSize;

        this._aIndex += aSize;
        this._iIndex += iSize;
    }

    _bindAndClearTexArray(texArray) {
        const textureSystem = this.renderer.texture;

        for (let j = 0; j < texArray.length; j++) {
            textureSystem.bind(texArray[j], j);
            texArray[j] = null;
        }
        texArray.length = 0;
    }

    _updateGeometry() {
        const {
            _packedGeometries: packedGeometries,
            _attributeBuffer: attributeBuffer,
            _indexBuffer: indexBuffer,
        } = this;

        if (!this.options.canUploadSameBuffer) { /* Usually on iOS devices, where the browser doesn't
            like uploads to the same buffer in a single frame. */
            if (this._packedGeometryPoolSize <= this._flushId) {
                this._packedGeometryPoolSize++;
                packedGeometries[this._flushId] = new (this.geometryClass)();
            }

            packedGeometries[this._flushId]._buffer.update(attributeBuffer.rawBinaryData);
            packedGeometries[this._flushId]._indexBuffer.update(indexBuffer);

            this.renderer.geometry.bind(packedGeometries[this._flushId]);
            this.renderer.geometry.updateBuffers();
            this._flushId++;
        }
        else {
            // lets use the faster option, always use buffer number 0
            packedGeometries[this._flushId]._buffer.update(attributeBuffer.rawBinaryData);
            packedGeometries[this._flushId]._indexBuffer.update(indexBuffer);

            this.renderer.geometry.updateBuffers();
        }
    }

    _drawBatches() {
        const dcCount = this._dcIndex;
        const { gl, state: stateSystem } = this.renderer;
        const drawCalls = this._drawCallPool;

        let curTexArray = null;

        // Upload textures and do the draw calls
        for (let i = 0; i < dcCount; i++) {
            const { texArray, type, size, start, blend } = drawCalls[i];

            if (curTexArray !== texArray) {
                curTexArray = texArray;
                this._bindAndClearTexArray(texArray);
            }

            this.state.blendMode = blend;
            stateSystem.set(this.state);
            gl.drawElements(type, size, gl.UNSIGNED_SHORT, start * 2);
        }
    }

    _flush() {
        const drawCall = this._drawCallPool[this._dcIndex];
        if (drawCall.size > 0)
            ++this._dcIndex;

        this._updateGeometry();
        this._drawBatches();

        this._flushReset();
    }

    _flushReset()
    {
        this._countTexArrays = 1;

        this._dcIndex = 0;
        this._aIndex = 0;
        this._iIndex = 0;
        this._vertexCount = 0;
        this._indexCount = 0;

        const drawCall = this._drawCallPool[0];

        drawCall.start = 0;
        drawCall.texArray = this._textureArrayPool[0];
        drawCall.blend = null;
        drawCall.size = 0;

        ++this._texArrayTick;
    }
}
