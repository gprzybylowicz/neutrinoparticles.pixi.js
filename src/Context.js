import * as PIXI from 'pixi.js';
import * as Neutrino from 'neutrinoparticles.js';

/**
 * Main NeutrinoParticles context for PIXI.
 * 
 * It has to be created before loading and using any effects in the application.
 * 
 * Usually, the context is created automatically by {@link ApplicationPlugin} after
 * PIXI.Application is created and assigned to PIXI.Application.neutrino property.
 * 
 * @param {PIXI.Renderer} renderer Current PIXI renderer.
 * @param {PIXI.Loader} loader A loader which will be used to load textures and effects.
 * @param {Object} [options={}] Options
 * @param {string} [options.texturesBasePath=""] This path will be added before any texture
 * path stored in a loaded effect.
 * @param {boolean} [options.trimmedExtensionsLookupFirst=true] Before loading any texture
 * during loading effect, will check if there is already loaded texture in the cache
 * with the same name but without extension. It is useful for atlases, as they have
 * sometimes textures without extensions in their description.
 * @param {number} [options.maxBatchVertices=16384] Maximum number of particles vertices
 * in a geometry batch. Several effects in a row can be batched together.
 * @param {number} [options.maxBatchTextures=PIXI.settings.SPRITE_MAX_TEXTURES] Maximum
 * number of different textures in a geometry batch. Several effects in a row can be
 * batched together.
 * @param {number} [options.canUploadSameBuffer=PIXI.settings.CAN_UPLOAD_SAME_BUFFER] If
 * we can use the same GPU geometry buffer to upload geometry. Controlled by PIXI.
 */

export class Context {

	constructor(renderer, options) {
		
		this.options = Object.assign({
            texturesBasePath: "",
            trimmedExtensionsLookupFirst: true
        }, options);

        /**
         * The context of 
         * {@link https://gitlab.com/neutrinoparticles/neutrinoparticles.js/ | neutrinoparticles.js} library.
         * @member {Neutrino.Context}
         */
		this.neutrino = new Neutrino.Context();

        /**
         * Shows if PIXI.CanvasRenderer used.
         * @member {boolean}
         */
        this.canvasRenderer = PIXI.CanvasRenderer ? 
            (renderer instanceof PIXI.CanvasRenderer) : false;

        /**
         * If noise already initialized in a some way.
         * @member {boolean}
         * @private
         */
        this._noiseInitialized = false;
        /**
         * In case if noise is being generated in multiple steps by generateNoiseStep(),
         * this will store NeutrinoParticles.NoiseGenerator until the process is finished.
         * @member {NeutrinoParticles.NoiseGenerator}
         * @private
         */
        this._noiseGenerator = null;
	}

    /**
     * Initiates loading of pre-generated noise file. This file you can find
     * in repository of 
     * {@link https://gitlab.com/neutrinoparticles/neutrinoparticles.js/ | neutrinoparticles.js} 
     * library. You will need to deploy this file with your application.
     * 
     * @param {string} path Path to the directory where 'neutrinoparticles.noise.bin' file
     * is located.
     * @param {function()} success Success callback.
     * @param {function()} fail Fail callback.
     */
	initializeNoise(path, success, fail) 
    {
        if (this._noiseInitialized)
        {
            if (success) success();
            return;
        }

        this.neutrino.initializeNoise(path, 
            function() 
            { 
                this._noiseInitialized = true; 
                if (success) success(); 
            }, 
            fail);
    }

    /**
     * Generates noise for effects in one call. It will lock script executing until finished.
     * 
     * This method should be called only once in the start of the application.
     */
	generateNoise()
    {
        if (this._noiseInitialized)
            return;

        let noiseGenerator = new this.neutrino.NoiseGenerator();
        while (!noiseGenerator.step()) { // approx. 5,000 steps
            // you can use 'noiseGenerator.progress' to get generating progress from 0.0 to 1.0
        }

        this._noiseInitialized = true;
    }
    
    /**
     * Generates noise in multiple steps. With this function you can spread noise
     * generation through multiple frames.
     * 
     * Generating noise should be made only once in the start of the application.
     * 
     * @example
     * let result;
     * do {
     *   result = app.neutrino.generateNoiseStep();
     *   const progress = result.progress; // Progress in range [0; 1]
     * 
     *   // do something with progress
     *   
     * } while (!result.done);
     */
    generateNoiseStep()
    {
        if (this._noiseInitialized)
        {
            return { done: true, progress: 1.0 };
        }

        if (!this._noiseGenerator)
            this._noiseGenerator = new this.neutrino.NoiseGenerator();

        const result = this._noiseGenerator.step();
        const _progress = this._noiseGenerator.progress;

        if (result)
        {
            this._noiseInitialized = true;
            this._noiseGenerator = null;
        }

        return { done: result, progress: _progress };
    }

    /**
     * Options for loader to hint resource as NeutrinoParticles effect. You has to
     * hint all NeutrinoParticles effects when requesting for load. Otherwise a plain JavaScript
     * text file will be loaded.
     * 
     * @example
     * app.loader.add('my_effect', 'path/to/my/effect.js', app.neutrino.loadOptions);
     */
	get loadOptions()
    {
        return { metadata: { neutrino: this } };
    }

}
