
/**
 * The class implements perspective projection transformation.
 * 
 * @example
 * app.loader
 * // Load effect using PIXI's loader. And don't forget to specify loadOptions
 * .add('my_effect', 'path/to/my/effect.js', app.neutrino.loadOptions)
 * .load((loader, resources) => 
 * {
 *   // Create effect instance using loaded model
 *   let effect = new PIXINeutrino.Effect(resources.my_effect.effectModel, {
 *     position: [400, 300, 0],
 *     projection: new PIXINeutrino.PerspectiveProjection(60.0)
 *   });
 *   // Add effect on the scene
 *   app.stage.addChild(effect);
 * 	 // Update effect with PIXI's ticker
 *   app.ticker.add((delta) => {
 * 	   const msec = delta / PIXI.settings.TARGET_FPMS;
 *     const sec = msec / 1000.0;
 * 	   effect.update(sec);
 *   });
 * });
 * 
 * @param {number} [horizontalAngle] Horizontal angle of the perspective projection in degrees.
 */

export class PerspectiveProjection
{
    constructor(horizontalAngle) {
        this.horizontalAngle = horizontalAngle;
    }

    /**
	 * Changes horizontal angle of the projection.
	 * 
	 * @param {number} value Angle in degrees.
	 */
    set horizontalAngle(value) {
        this._horizontalAngle = value;
        this._angleTan = Math.tan((value * 0.5) / 180.0 * Math.PI);
    }

    /**
     * Sets rendering frame for the projection.
     * 
     * This method shouldn't be called manually when the class is used with effects. It is called
     * on every render call for each effect automatically.
     * @param {PIXI.Rectangle} frame Rendering frame.
     */
    setScreenFrame(frame) {
        this._screenWidth = frame.width;
        this._screenHeight = frame.height;
        this._screenPosX = frame.x + frame.width * 0.5;
        this._screenPosY = frame.y + frame.height * 0.5;
        this._z = this._screenWidth * 0.5 / this._angleTan;
        this._near = this._z * 0.99;
    }

    /**
     * Transforms 3D point accordingly to the projection.
     * 
     * Basically, point's position X and Y components are simply scaled dependently on Z position. The method
     * is used in WebGL and Canvas rendering.
     * 
     * @param {Array} out [x, y] Transformed position.
     * @param {Array} pos [x, y, z] Untransformed input vertex position.
     * @returns false, if a particle is on the back side of the camera and should be discarded. Otherwise - true.
     */
    transformPosition(out, pos) {
        if (pos[2] > this._near) {
            return false;
        }

        const scale = this._getScale(pos);
        out[0] = (pos[0] - this._screenPosX) * scale + this._screenPosX;
        out[1] = (pos[1] - this._screenPosY) * scale + this._screenPosY;

        return true;
    }

    /**
     * Transforms 2D size of a particle accordingly to the projection.
     * 
     * Basically, size is simply scaled dependently on Z position of a particle. The method is used
     * only in Canvas rendering.
     * 
     * @param {Array} outSize [width, height] Transformed size.
     * @param {Array} pos [x, y, z] Untransformed particle position.
     * @param {Array} size [width, height] Untransformed size.
     */
    transformSize(outSize, pos, size) {
        const scale = this._getScale(pos);
        outSize[0] = size[0] * scale;
        outSize[1] = size[1] * scale;
    }

    _getScale(pos) {
        return this._z / (this._z - pos[2]);
    }
}