function TestUpdateRender(ctx)
{
    describe('UpdateRender', function(cb) {
        it("RenderBeforeFirstUpdate", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/positioning.js', {
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.check();
                ibct.finalize(cb);
            }
        });
    });
}

export {
    TestUpdateRender
}