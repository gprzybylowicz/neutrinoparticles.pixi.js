export function TestBaseParent(ctx)
{
    describe('BaseParent', function(cb) {

        it("Moving", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/base_parent.js', {
                    position: [200, 100, 0]
                })
            `);

            function runTest() {
                ibct.exec(`
                    let baseParent = new PIXI.Container();
                    app.stage.addChild(baseParent);
                    baseParent.addChild(test.effect);
                    test.effect.baseParent = baseParent;
                `);
                ibct.updateAndCheck(2);
                ibct.exec(`test.effect.parent.position = new PIXI.Point(400, 300);`)
                ibct.updateAndCheck(0);

                ibct.finalize(cb);
            }
        });

        it("Rotating", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/base_parent.js', { 
                    position: [200, 0, 0]
                });
            `);

            function runTest() {
                ibct.exec(`
                    let baseParent = new PIXI.Container();
                    baseParent.rotation = Math.PI / 4;
                    baseParent.position = new PIXI.Point(400, 300);
                    app.stage.addChild(baseParent);
                    baseParent.addChild(test.effect);
                    test.effect.baseParent = baseParent;
                `);
                ibct.updateAndCheck(2);
                ibct.exec(`test.effect.parent.rotation = Math.PI / 2;`)
                ibct.updateAndCheck(0);

                ibct.finalize(cb);
            }
        });

        it("Scaling", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/base_parent.js', { 
                    position: [200, 100, 0]
                })
            `);

            function runTest() {
                ibct.exec(`
                    let baseParent = new PIXI.Container();
                    app.stage.addChild(baseParent);
                    baseParent.addChild(test.effect);
                    test.effect.baseParent = baseParent;
                `)
                ibct.updateAndCheck(2);
                ibct.exec(`test.effect.parent.scale = new PIXI.Point(2, 2);`)
                ibct.updateAndCheck(0);

                ibct.finalize(cb);
            }
        });
    });
}
