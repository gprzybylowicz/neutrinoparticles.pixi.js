const ipc =  require('electron').ipcRenderer;
const path = require('path');
const sinon = require('sinon');
const assert = require('assert');

window.onerror = function(error)
{
  ipc.send('error', error);
}

let app, test = null;

PIXINeutrino.registerPlugins();

let reset = function(options)
{
  test = {};

  if (app)
  {
    document.body.removeChild(app.view);
    app.destroy();
    //PIXI.loader.destroy();
    PIXI.utils.destroyTextureCache();
  }

  app = new PIXI.Application(options);

  document.body.appendChild(app.view);
}

let loadEffect = function(path, options)
{
  app.loader.add('test_effect', path, app.neutrino.loadOptions)
      .load((loader, resources) => 
  {
      test.effect = new PIXINeutrino.Effect(resources.test_effect.effectModel,
        options);
      app.stage.addChild(test.effect);

      test.update = function(sec) 
      { 
        test.effect.update(sec); 
      }
      runTest();
  });
}

let render = function()
{
  app.render();
  return app.view.toDataURL();
}

let rad = function(deg)
{
  return Math.PI / 180 * deg;
}

let runTest = function()
{
  ipc.send('effects-ready');
}