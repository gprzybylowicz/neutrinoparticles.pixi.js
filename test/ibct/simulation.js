import * as path from 'path';
import * as fs from 'fs';

function fsFriendly(p) {
    return p.replace(/[^a-z0-9]/gi, '_').toLowerCase();
}

function TestSimulation(ctx)
{
    function TestBySimpleSimulationFromDir(relEffectsDir, generateNoise)
    {
        const effectsDir = path.join(__dirname, relEffectsDir);
        const effectsFiles = fs.readdirSync(effectsDir);

        for (let i = 0; i < effectsFiles.length; ++i) {
            const effectFile = path.join(effectsDir, effectsFiles[i]);

            if (fs.statSync(effectFile).isDirectory())
            continue;

            const testTitle = fsFriendly(path.join(relEffectsDir, effectsFiles[i]));
            it (testTitle, function(cb) {
                const ibct = ctx.ibct;

                ibct.reset(this.test.fullTitle(), {}, runTest);
            
                if (generateNoise)
                    ibct.exec(`app.neutrino.generateNoise()`);

                ibct.exec(`
                    loadEffect('` + path.join(relEffectsDir, effectsFiles[i]) + `', {
                        position: [400, 300, 0]
                    })
                `);

                function runTest() {
                    const updates = [0.1, 0.2, 0.3, 0.4, 5];
                    updates.forEach((dt) => {
                        ibct.updateAndCheck(dt);
                    });
                    
                    ibct.finalize(cb);
                }
            });
        }
    }

    describe('Simulation', function(cb) {
        TestBySimpleSimulationFromDir("effects/simulation", false);
        TestBySimpleSimulationFromDir("effects/turbulence", true);
    });
}

export {
    TestSimulation
}