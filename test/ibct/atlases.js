function TestAtlases(ctx)
{
    describe('Atlases', function(cb) {
        it('Simple', function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);

            ibct.exec(`
                app.loader.add('textures/atlas.json').load(() =>
                {
                    loadEffect('effects/atlas/2emitters.js', {
                        position: [400, 300, 0]
                    });
                })
            `);

            function runTest() {
                ibct.updateAndCheck(4);

                ibct.finalize(function(err) {
                    if (err) {
                        cb(err);
                        return;
                    }

                    // Check that all textures of the effect are from the same base texture (atlas)
                    ibct.exec(`
                        let model = test.effect.effectModel;
                        let texture = model.textures[0].basicTexture;
                        let result = true;
                        for (let i = 1; i < model.textures.length; ++i) {
                            if (model.textures[i].basicTexture != texture) {
                                result = false;
                                break;
                            }
                        }
                        return result;
                    `).then(function(result) {
                        if (result)
                            cb();
                        else
                            cb("Textures used on effect are not from the same atlas!");
                    })
                });
            }
        });
    });
}

export {
    TestAtlases
}