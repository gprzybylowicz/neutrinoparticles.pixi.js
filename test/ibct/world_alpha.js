function TestWorldAlpha(ctx)
{
    describe('WorldAlpha', function(cb) {

        it("Normal", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);

            ibct.exec(`
                loadEffect('effects/world_alpha.js', {
                    position: [400, 300, 0],
                    projection: new PIXINeutrino.PerspectiveProjection(60)
                })
            `);

            function runTest() {
                ibct.updateAndCheck(1);
                ibct.finalize(cb);
            }
        });

        it("0.7", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);

            ibct.exec(`
                loadEffect('effects/world_alpha.js', {
                    position: [400, 300, 0],
                    projection: new PIXINeutrino.PerspectiveProjection(60)
                })
            `);

            function runTest() {
                ibct.exec(`test.effect.alpha = 0.7`);
                ibct.updateAndCheck(1);
                ibct.finalize(cb);
            }
        });

        it("Zero", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);

            ibct.exec(`
                loadEffect('effects/world_alpha.js', {
                    position: [400, 300, 0],
                    projection: new PIXINeutrino.PerspectiveProjection(60)
                })
            `);

            function runTest() {
                ibct.exec(`test.effect.alpha = 0.0`);
                ibct.updateAndCheck(1);
                ibct.finalize(cb);
            }
        });

    });
}

export {
    TestWorldAlpha
}