function TestScaling(ctx)
{
    describe('Scaling', function(cb) {
        it("Uniform", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
            app.loader.add('test_effect', 'effects/scaling.js', app.neutrino.loadOptions)
                .load((loader, resources) => 
                {
                    test.effect1 = new PIXINeutrino.Effect(
                        resources.test_effect.effectModel, {
                            position:[100, 100, 0],
                            scale: [1, 1, 1]
                        });
                    app.stage.addChild(test.effect1);

                    test.effect2 = new PIXINeutrino.Effect(
                        resources.test_effect.effectModel, {
                            position:[600, 200, 0],
                            scale: [2, 2, 2]
                        });
                    app.stage.addChild(test.effect2);

                    test.effect3 = new PIXINeutrino.Effect(
                        resources.test_effect.effectModel, {
                            position:[300, 400, 0],
                            scale: [4, 4, 4]
                        });
                    app.stage.addChild(test.effect3);

                    test.update = function(sec) 
                    { 
                        test.effect1.update(sec); 
                        test.effect2.update(sec); 
                        test.effect3.update(sec); 
                    }
                    runTest();
                });
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.finalize(cb);
            }
        });

        it("NonUniform", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
            app.loader.add('test_effect', 'effects/scaling.js', app.neutrino.loadOptions)
                .load((loader, resources) => 
                {
                    test.effect1 = new PIXINeutrino.Effect(
                        resources.test_effect.effectModel, {
                            position:[200, 200, 0],
                            scale: [1, 2, 1]
                        });
                    app.stage.addChild(test.effect1);

                    test.effect2 = new PIXINeutrino.Effect(
                        resources.test_effect.effectModel, {
                            position:[600, 400, 0],
                            scale: [2, 1, 1]
                        });
                    app.stage.addChild(test.effect2);

                    test.update = function(sec) 
                    { 
                        test.effect1.update(sec); 
                        test.effect2.update(sec); 
                    }
                    runTest();
                });
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.finalize(cb);
            }
        });
    });
}

export {
    TestScaling
}