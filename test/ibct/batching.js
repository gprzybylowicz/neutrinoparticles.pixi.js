function TestBatching(ctx)
{
    describe('Batching', function(cb) {

        it("GeometryWithoutOverflow", function(cb) {
            const ibct = ctx.ibct;

            // Exact number of vertices used
            ibct.reset(this.test.fullTitle(), 
                { neutrino: { maxBatchTextures: 2000 }}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                loadEffect('effects/batching/five_textures.js', { 
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 1)
                `)
                ibct.finalize(cb);
            }
        });

        it("GeometryOverflow", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), 
                { neutrino: { maxBatchVertices: 500 }}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                app.renderer.geometry.bind = sinon.fake(
                    app.renderer.geometry.bind);

                loadEffect('effects/batching/five_textures.js', { 
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 4);
                    assert.equal(app.renderer.geometry.bind.callCount, 1);
                `)
                ibct.finalize(cb);
            }
        });

        it("GeometryOverflowNonSameBuffer", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), 
                { neutrino: { maxBatchVertices: 500, canUploadSameBuffer: false }}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                app.renderer.geometry.bind = sinon.fake(
                    app.renderer.geometry.bind);

                loadEffect('effects/batching/five_textures.js', { 
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 4);
                    assert.equal(app.renderer.geometry.bind.callCount, 4);
                `)
                ibct.finalize(cb);
            }
        });

        it("TexturesOverflow", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), 
                { neutrino: { maxBatchVertices: 2000, maxBatchTextures: 2 }}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                loadEffect('effects/batching/five_textures.js', { 
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 3)
                `)
                ibct.finalize(cb);
            }
        });

        function TestSeveralEffects(maxVertices, callCount, cb)
        {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), 
                { neutrino: { maxBatchVertices: maxVertices }}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                app.loader.add('test_effect', 'effects/batching/five_textures.js', app.neutrino.loadOptions)
                .load((loader, resources) => 
                {
                    test.effect1 = new PIXINeutrino.Effect(
                        resources.test_effect.effectModel, {
                            position:[200, 150, 0]
                        });
                    app.stage.addChild(test.effect1);

                    test.effect2 = new PIXINeutrino.Effect(
                        resources.test_effect.effectModel, {
                            position:[600, 450, 0]
                        });
                    app.stage.addChild(test.effect2);

                    test.update = function(sec) 
                    { 
                        test.effect1.update(sec); 
                        test.effect2.update(sec); 
                    }
                    runTest();
                });
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, ` + callCount + `)
                `)
                ibct.finalize(cb);
            }
        }

        it("SeveralEffects", function(cb) {
            TestSeveralEffects.call(this, 4000, 1, cb);
        });
        it("SeveralEffectsMinusOneVertex", function(cb) {
            TestSeveralEffects.call(this, 3999, 2, cb);
        });

        it("Blending", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), 
                { neutrino: { maxBatchVertices: 2000 }}, runTest);
            
            ibct.exec(`
                app.renderer.gl.drawElements = sinon.fake(
                    app.renderer.gl.drawElements);

                loadEffect('effects/batching/five_textures_blended.js', { 
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.updateAndCheck(2);
                ibct.exec(`
                    assert.equal(app.renderer.gl.drawElements.callCount, 5)
                `)
                ibct.finalize(cb);
            }
        });
    });
}

export {
    TestBatching
}