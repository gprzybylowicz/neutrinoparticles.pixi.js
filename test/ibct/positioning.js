function TestPositioning(ctx)
{
    describe('Positioning', function(cb) {

        it("Static", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);

            ibct.exec(`
                loadEffect('effects/positioning.js', {
                    position: [400, 300, 0]
                })
            `);

            function runTest() {
                ibct.check();
                ibct.updateAndCheck(4);
                    
                ibct.finalize(cb);
            }
        });

        it("Moving", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/positioning.js', {
                    position: [0, 0, 0]
                })
            `);

            function runTest() {
                ibct.check();
                ibct.exec(`test.effect.position = new PIXI.Point(400, 300)`);
                ibct.updateAndCheck(2.0);
                    
                ibct.finalize(cb);
            }
        });

        it("MovingAndJump", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/positioning.js', {
                    position: [0, 0, 0]
                })
            `);

            function runTest() {
                ibct.check();
                ibct.exec(`test.effect.position = new PIXI.Point(200, 150);`);
                ibct.update(1.0);
                ibct.exec(`
                    test.effect.resetPosition([400, 300, 0]); 
                    test.effect.position = new PIXI.Point(600, 450);`);
                ibct.updateAndCheck(1.0);
                    
                ibct.finalize(cb);
            }
        });

    });
}

export {
    TestPositioning
}