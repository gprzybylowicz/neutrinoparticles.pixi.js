function TestSceneHierarchy(ctx)
{
    describe('SceneHierarchy', function(cb) {

        // TODO: +Rotating parent
        // TODO: +Rotating parent and child

        it("StaticParent", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/scene_hierarchy.js', {
                    position: [0, 0, 0] 
                })
            `);

            function runTest() {
                ibct.exec(`
                    let parent = new PIXI.Container();
                    parent.position = new PIXI.Point(400, 300);
                    app.stage.addChild(parent);
                    parent.addChild(test.effect);
                `);
                ibct.updateAndCheck(2);
                ibct.finalize(cb);
            }
        });

        it("RotatingParent", function(cb) {
            const ibct = ctx.ibct;

            ibct.reset(this.test.fullTitle(), {}, runTest);
            
            ibct.exec(`
                loadEffect('effects/scene_hierarchy.js', { 
                    position: [150, 0, 0] 
                })
            `);

            function runTest() {
                ibct.exec(`
                    let parent = new PIXI.Container();
                    parent.position = new PIXI.Point(400, 300);
                    app.stage.addChild(parent);
                    parent.addChild(test.effect);
                `);
                ibct.check();
                ibct.exec(`test.effect.parent.rotation = 45`);
                ibct.updateAndCheck(2);
                ibct.finalize(cb);
            }
        });
        
    });
}

export {
    TestSceneHierarchy
}