## v2.3.3
* Fixed perspective projection for scaled effect.

## v2.3.2
* Respect world alpha of the container.

## v2.3.1
* Z axis of perspective projection swapped to look backward.

## v2.3.0
* Perspective projection.

## v2.2.1
* TAR archive on release page instead of plain .js.

## v2.2.0
* EffectModel interface changed to accept effect source and abstract TextureLoader.

## v2.1.3
* Own batch renderer for particles geometry.

## v2.1.2
* Syncronized to internal PIXI v5.2.0 changes _id -> _batchPosition

## v2.1.1
* Switched to neutrinoparticles.js@2.0.1

## v2.1.0
* Pause added to constructor of Effect.

## v2.0.0
* Sources ported to PIXI5.
* Tests, samples and documentation adjusted.

## v1.0.0
* PIXI4 rendering plugin.
* Samples.
* Basic Usage tutorial.

## v0.0.1
* Testing of release message.
